title: のりもん
date: 2019-07-28 11:48:08
---
# Car
## Suzuki Jimny JB23W('00)
<img src="https://i.ibb.co/NrWx1J1/IMG-20191113-091401.jpg" alt="IMG-20191113-091401" border="0">

<img src="https://i.ibb.co/tPJ5SQR/IMG-20191113-091420.jpg" alt="IMG-20191113-091420" border="0">

### 装備
- 3インチアップ
- Androidナビ
- でかいタイヤ
- 20年目の貫禄


# Motorbikes

## Suzuki Djebel 250XC ('01-'04)
![じぇべる](https://i.ibb.co/8gN4H39/IMG-2790.jpg)

念願の250CCオフロードバイク。  

超かわいいルックスで通行人の目線を独り占めっ♡  

### 装備
- 箱  
ホムセン箱。自作ステーでボルトどめなので、5分でつけ外し可能。  
帰省等大荷物の時以外には外している。

- 電源  
デイトナUSB電源。

- 板ホルダー  
スマホホルダー。

## Honda NS-1 ('91)
![えぬわん](https://live.staticflickr.com/7907/46734516451_02d30e1c63_c.jpg)

人生初愛車。

超うるさいエンジン音で通行人のジト目を独り占めっ♡

### 装備
- 電源  
中華USB電源。

- 板ホルダー  
スマホホルダー。

\* 2019/09/26 盗難