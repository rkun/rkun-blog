title: Mastodonの利用者を制限する方法
author: rkun
date: 2019-06-12 12:43:39
tags:
---
[Kyutodon](mstdn.rkunkunr.com)みたいな、
適当なコミュニティでMastodonインスタンスの利用を制限したいとき、どうするかという話。  

まあ識別する方法としては
- メルアドフィルタリング  
`awesomuniversity.ac.jp`みたいなドメインでフィルタリングする。
- IPアドレスフィルタリング  
``