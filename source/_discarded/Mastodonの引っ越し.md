title: Mastodonの引っ越し
author: rkun
date: 2019-06-14 10:49:20
tags:
---
Mastodonのお引越しをしているのでざっとメモ

## 環境
### 移行元
|HostName|sawachan|
|:-:|:-:|
|VPS|ConoHa 1GB SSD|
|OS|Ubuntu 16.04.1 LTS|
|Mastodon|2.8.2|
|Docker?|**Non-Docker**|

### 移行先
|HostName|amane|
|:-:|:-:|
|VPS|Vultr 2GB|
|OS|Arch Linux|
|Mastodon|2.8.2(移行元と同じ)|
|Docker?|**Docker**|

## 作業
1. 移行先サーバーの準備  
いつもどおりMastodonをセットアップしておく。私の場合まっさらなMastodon建ててexp.mstdn.rkunkunr.comでテスト鯖として稼働させるまでやりました。

2. データベースのDump  
移行元サーバーにて
`pg_dump mastodon_production > mastodon_production_postgress`  

3. アセットのバックアップ  
静的なアセット（画像なんかも）をバックアップします。
移行元サーバーにて、
`cp -r live/public mastodon_public`

4. 移行元 -> 移行先へコピー  
特にAssetフォルダーは