title: Geforce GTX1060 on HP Z400
author: rkun
date: 2018-12-15 11:32:53
tags:
---
> "冬といえばゲーム  
ゲームといえばグラボ"  

というわけで，グラボ買いました．
てかその前に私のデスクトップPCを紹介しないといけませんよね．

えっと，**HP Z400**です．

結構古いワークステーションですね．
私の個体は

|||
|--:|:--:|
|CPU|Intel Xeon W3580|
|RAM|12GB|
|ストレージ|SSD 256GB + HDD 250GB|
|GPU|Nvidia Quadro FX1800|
な感じでした．  
グラボ積んでんじゃんってツッコミは無効です．  
なぜならこのグラボがショボタンだからですね．

いまじゃヤフオクで千円前後で流通してるQuadroなわけで，性能も御察し．  
頑張ったんだけどだめだった．

そして我慢できず1060をぽちったわけですね．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/44503044560/in/dateposted-public/" title="IMG_2673"><img src="https://farm5.staticflickr.com/4881/44503044560_d88d9d1875_k.jpg" width="2048" height="1365" alt="IMG_2673"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

買ったのは玄人志向の1060でVRAMが6GBのモデルですね．\25000ほどでした．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/44503185600/in/dateposted-public/" title="IMG_2674"><img src="https://farm5.staticflickr.com/4895/44503185600_02c3df3371_k.jpg" width="2048" height="1365" alt="IMG_2674"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/44503045500/in/dateposted-public/" title="IMG_2678"><img src="https://farm5.staticflickr.com/4903/44503045500_d674817f65_k.jpg" width="2048" height="1365" alt="IMG_2678"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

流石ワークステーションですね．このノブを引き上げて後ろに押すとすぐにカバーが外れます．

中をみるとFX1800が蜘蛛の巣張られて佇んでました.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/45406882045/in/dateposted-public/" title="IMG_2679"><img src="https://farm5.staticflickr.com/4837/45406882045_c739789604_k.jpg" width="2048" height="1365" alt="IMG_2679"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

PCIEのバックパネルはこのノブを...

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/45406884235/in/dateposted-public/" title="IMG_2684"><img src="https://farm5.staticflickr.com/4895/45406884235_d187bb7e61_k.jpg" width="2048" height="1365" alt="IMG_2684"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

外側に押し出します.下側も同様.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/32447584538/in/dateposted-public/" title="IMG_2685"><img src="https://farm5.staticflickr.com/4844/32447584538_150e21be58_k.jpg" width="2048" height="1365" alt="IMG_2685"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

すっきり.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/138671784@N08/45406885085/in/dateposted-public/" title="IMG_2686"><img src="https://farm5.staticflickr.com/4829/45406885085_cd5b9d87c6_k.jpg" width="2048" height="1365" alt="IMG_2686"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

肝心の感想ですが,めちゃくちゃに良いです.  
1060は不満が出るかと思いましたが,私の持ってる積みゲーたちは基本最高設定でも50fps超えてましたし,特に不満はありません.  
特に,**ETS2**の運転しやすさはピカイチですね.  

さて今回は写真をFlickrに上げて埋め込んでみました.  
Flickrへのアップロードは手間もかからなくて便利です.  

Flickrの画像サーバー(farm5.staticflickr.com)が日本の近くにあってくれることを祈るのみです.