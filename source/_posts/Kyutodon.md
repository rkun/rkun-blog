title: Kyutodon
author: rkun
tags: []
categories: []
date: 2019-05-30 00:25:00
---
## [Kyutodon](https://mstdn.rkunkunr.com)

建てました。Mastodonです。  
九工大情報工学部の学生にのみアカウント新規登録を開放しています。  

どうやって判断しようか迷ったんですが、最終的にIPアドレスでフィルタリングする形にしてみました。  
いろんな人に、
>nginxでリバースプロキシ組んでるんだったらnginxでIPフィルタリングすればいいじゃん！！

とかいわれたんですが、アクセス自体遮断するならまだしもアカウント新規登録だけ遮断したいし、フォームをグレイアウトさせたりRailsの中身に関わる部分をいじりたかったので、[Folkしました](https://gitlab.com/rkun/kyutech-mastodon)。

さあ、規模についてですが、

<a href="https://ibb.co/XtxSvjV"><img src="https://i.ibb.co/hVFDtLC/Screenshot-from-2019-06-11-17-05-08.png" alt="Screenshot-from-2019-06-11-17-05-08" border="0"></a><br /><a target='_blank' href='https://freeonlinedice.com/'></a>

こんな感じです。

まあ基本しゃべってるのは僕[@rkun](mstdn.rkunkunr.com/rkun)と[@prokuma](mstdn.rkunkunr.com/prokuma)ですがww

さあ画面の前の皆さん。ぜひご参加ください！！