#!/usr/bin/bash

echo "Start deploying..."

git add .
git commit -m $(date "+%Y%m%d-%H%M%S")
git push origin master

echo "Finished."
